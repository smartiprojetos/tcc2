﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Utilitarios
{
    class BuscarCep
    {
       
        public void Buscarcep (string cep)
            {
            // Lê e formata o CEP do textbox
            string cep2 = cep.Trim().Replace("-", "");

            // Chama função BuscarAPICorreio que irá pegar o cep já formatado e procurar na base
            CorreioResponse correio = BuscarAPICorreio(cep2);
                     
        }


        public CorreioResponse BuscarAPICorreio(string cep)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Chama API do correio, concatenando o cep
            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            // Transforma a resposta do correio em DTO
            CorreioResponse correio = JsonConvert.DeserializeObject<CorreioResponse>(resposta);
            return correio;
        }
    }
}
