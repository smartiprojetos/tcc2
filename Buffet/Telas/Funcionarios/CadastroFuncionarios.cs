﻿using Buffet.Db.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroFuncionarios : Form
    {
        public CadastroFuncionarios()
        {
            InitializeComponent();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {

            DTO_Fucionario funcionario = new DTO_Fucionario();
            funcionario.Nome = txtnome.Text;
            funcionario.CPF = txtcpf.Text;
            funcionario.Data_Cadastro = dtpcadastro.Value;
            funcionario.Situacao = cbosituacao.SelectedItem.ToString();
            funcionario.Data_Saida = dtpdatasaida.Value;

            //arrumar isso aqui
            //funcionario.fk_cargo = cbocargo.SelectedItem.ToString();
            funcionario.observacao_Cadastro = txtobservacaofuncionarios.Text;
            funcionario.CEP = txtcep.Text;
            funcionario.UF = cbouf.SelectedItem.ToString();
            funcionario.cidade = txtcidade.Text;
            funcionario.endereco = txtendendereco.Text;
            funcionario.complemento = txtcomplemento.Text;
            funcionario.bairro = txtbairro.Text;
            funcionario.Numero = txtnumero.Text;
            funcionario.observacoes_Endereco = txtobservacoesendereco.Text;
            funcionario.telefone = txttelefone.Text;
            funcionario.celular = txtcelular.Text;
            funcionario.email = txtemail.Text;
            funcionario.user_usuario = txtusuario.Text;
            funcionario.sn_senha = txtsenha.Text;

           // if(rdnadm.Checked==true)
           // {
           //     funcionario.pr_permissao_cliente = true;
           //     funcionario.pr_permissao_cadastrar_cliente = true;
           //     funcionario.pr_permissao_consultar_cliente = true;

           //     funcionario.pr_permissao_atualiza_cadastror_cliente = true;
           //     funcionario.pr_permissao_pedido = true;

           //     funcionario.pr_permissao_novo_pedido = true;
           //     funcionario.pr_permissao_atualizar_pedido = true;
           //     funcionario.pr_permissao_consultar_pedido = true;
           //     funcionario.pr_permissao_funcionario = true;
           //     funcionario.pr_permissao_cadastrar_funcionario = true;
           //     funcionario.pr_permissao_novo_funcionario = true;
           //     funcionario.pr_permissao_alterar_dados_funcionarios = true;
           //     funcionario.pr_permissao_consultar_funcionarios = true;
           //     funcionario.pr_permissao_fornecedor = true;
           //     funcionario.pr_permissao_cadastrar_fornecedor = true;
           //     funcionario.pr_permissao_consultar_fornecedor = true;
           //     funcionario.pr_permissao_estoque = true;
           //     funcionario.pr_permissao_consultar_estoque = true;
           //     funcionario.pr_permissao_alterar_estoque = true;
           //     funcionario.pr_permissao_perdir_ao_fornecedor = true;
           //     funcionario.pr_permissao_cadastrar_terceiriziados = true;
           //     funcionario.pr_permissão_consultar_terceriziado = true;
           //     funcionario.pr_permisssao_terceirizados = true;
           // }

           //if (rdnsempermissao.Checked == true)
           // {
           //     funcionario.pr_permissao_cliente = false;
           //     funcionario.pr_permissao_cadastrar_cliente = false;
           //     funcionario.pr_permissao_consultar_cliente = false;

           //     funcionario.pr_permissao_atualiza_cadastror_cliente = false;
           //     funcionario.pr_permissao_pedido = false;

           //     funcionario.pr_permissao_novo_pedido = false;
           //     funcionario.pr_permissao_atualizar_pedido = false;
           //     funcionario.pr_permissao_consultar_pedido = false;
           //     funcionario.pr_permissao_funcionario = false;
           //     funcionario.pr_permissao_cadastrar_funcionario = false;
           //     funcionario.pr_permissao_novo_funcionario = false;
           //     funcionario.pr_permissao_alterar_dados_funcionarios = false;
           //     funcionario.pr_permissao_consultar_funcionarios = false;
           //     funcionario.pr_permissao_fornecedor = false;
           //     funcionario.pr_permissao_cadastrar_fornecedor = false;
           //     funcionario.pr_permissao_consultar_fornecedor = false;
           //     funcionario.pr_permissao_estoque = false;
           //     funcionario.pr_permissao_consultar_estoque = false;
           //     funcionario.pr_permissao_alterar_estoque = false;
           //     funcionario.pr_permissao_perdir_ao_fornecedor = false;
           //     funcionario.pr_permissao_cadastrar_terceiriziados = false;
           //     funcionario.pr_permissão_consultar_terceriziado = false;
           //     funcionario.pr_permisssao_terceirizados = false;

           // }

            Business_Funcionario business = new Business_Funcionario();
            business.Salvar(funcionario);

            MessageBox.Show("Funcionário cadastrado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
           

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funcionário alterado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void label21_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            DTO_Fucionario funcionario = new DTO_Fucionario();
            int fk = UserSession.UsuarioLogado.fk_funcionario;
            funcionario.Nome = txtnome.Text;
            funcionario.CPF = txtcpf.Text;
            funcionario.Data_Cadastro = dtpcadastro.Value;
            funcionario.Situacao = cbosituacao.SelectedItem.ToString();
            funcionario.Data_Saida = dtpdatasaida.Value;
            //arrumar isso aqui também
            //funcionario.fk_cargo = cbocargo.SelectedItem.ToString();
            funcionario.observacao_Cadastro = txtobservacaofuncionarios.Text;
            funcionario.CEP = txtcep.Text;
            funcionario.UF = cbouf.SelectedItem.ToString();
            funcionario.cidade = txtcidade.Text;
            funcionario.endereco = txtendendereco.Text;
            funcionario.complemento = txtcomplemento.Text;
            funcionario.bairro = txtbairro.Text;
            funcionario.Numero = txtnumero.Text;
            funcionario.observacoes_Endereco = txtobservacoesendereco.Text;
            funcionario.telefone = txttelefone.Text;
            funcionario.celular = txtcelular.Text;
            funcionario.email = txtemail.Text;
            funcionario.user_usuario = txtusuario.Text;
            funcionario.sn_senha = txtsenha.Text;

            //if (rdnadm.Checked == true)
            //{
            //    funcionario.pr_permissao_cliente = true;
            //    funcionario.pr_permissao_cadastrar_cliente = true;
            //    funcionario.pr_permissao_consultar_cliente = true;

            //    funcionario.pr_permissao_atualiza_cadastror_cliente = true;
            //    funcionario.pr_permissao_pedido = true;

            //    funcionario.pr_permissao_novo_pedido = true;
            //    funcionario.pr_permissao_atualizar_pedido = true;
            //    funcionario.pr_permissao_consultar_pedido = true;
            //    funcionario.pr_permissao_funcionario = true;
            //    funcionario.pr_permissao_cadastrar_funcionario = true;
            //    funcionario.pr_permissao_novo_funcionario = true;
            //    funcionario.pr_permissao_alterar_dados_funcionarios = true;
            //    funcionario.pr_permissao_consultar_funcionarios = true;
            //    funcionario.pr_permissao_fornecedor = true;
            //    funcionario.pr_permissao_cadastrar_fornecedor = true;
            //    funcionario.pr_permissao_consultar_fornecedor = true;
            //    funcionario.pr_permissao_estoque = true;
            //    funcionario.pr_permissao_consultar_estoque = true;
            //    funcionario.pr_permissao_alterar_estoque = true;
            //    funcionario.pr_permissao_perdir_ao_fornecedor = true;
            //    funcionario.pr_permissao_cadastrar_terceiriziados = true;
            //    funcionario.pr_permissão_consultar_terceriziado = true;
            //    funcionario.pr_permisssao_terceirizados = true;
            //}

            //if (rdnsempermissao.Checked == true)
            //{
            //    funcionario.pr_permissao_cliente = false;
            //    funcionario.pr_permissao_cadastrar_cliente = false;
            //    funcionario.pr_permissao_consultar_cliente = false;

            //    funcionario.pr_permissao_atualiza_cadastror_cliente = false;
            //    funcionario.pr_permissao_pedido = false;

            //    funcionario.pr_permissao_novo_pedido = false;
            //    funcionario.pr_permissao_atualizar_pedido = false;
            //    funcionario.pr_permissao_consultar_pedido = false;
            //    funcionario.pr_permissao_funcionario = false;
            //    funcionario.pr_permissao_cadastrar_funcionario = false;
            //    funcionario.pr_permissao_novo_funcionario = false;
            //    funcionario.pr_permissao_alterar_dados_funcionarios = false;
            //    funcionario.pr_permissao_consultar_funcionarios = false;
            //    funcionario.pr_permissao_fornecedor = false;
            //    funcionario.pr_permissao_cadastrar_fornecedor = false;
            //    funcionario.pr_permissao_consultar_fornecedor = false;
            //    funcionario.pr_permissao_estoque = false;
            //    funcionario.pr_permissao_consultar_estoque = false;
            //    funcionario.pr_permissao_alterar_estoque = false;
            //    funcionario.pr_permissao_perdir_ao_fornecedor = false;
            //    funcionario.pr_permissao_cadastrar_terceiriziados = false;
            //    funcionario.pr_permissão_consultar_terceriziado = false;
            //    funcionario.pr_permisssao_terceirizados = false;

            //}

            Business_Funcionario business = new Business_Funcionario();
            business.Salvar(funcionario);

            MessageBox.Show("Funcionário cadastrado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funcionário alterado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void dtpdatasaida_ValueChanged(object sender, EventArgs e)
        {

        }

        private void CadastroFuncionarios_Load(object sender, EventArgs e)
        {

        }
    }
}
