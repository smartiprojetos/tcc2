﻿using Buffet.Db.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarClientes : Form
    {
        public ConsultarClientes()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Busca();
        }

        Business_Cliente bus = new Business_Cliente();
        DTO_Clientes dto = new DTO_Clientes();

        private void Busca()
        {
            if (txtbusca.Text == string.Empty)
            {
                bus.Listar();
                List<DTO_Clientes> lista = bus.Listar();
                dgvconsultarclientes.AutoGenerateColumns = false;
                dgvconsultarclientes.DataSource = lista;
            }
            else
            {
                bus.Consultar(txtbusca.Text);
                List<DTO_Clientes> lista = bus.Consultar(txtbusca.Text);
                dgvconsultarclientes.AutoGenerateColumns = false;
                dgvconsultarclientes.DataSource = lista;
            }

        }

        private void dgvconsultarclientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Se a coluna 5 -- 
            if (e.ColumnIndex == 5)
            {
                //pega os valores da linha selecionada e envia para o dto 
                DTO_Clientes dto = dgvconsultarclientes.CurrentRow.DataBoundItem as DTO_Clientes;
                bus.Remover(dto.ID);
            }
            if (e.ColumnIndex == 4)
            {
                 DTO_Clientes dto = dgvconsultarclientes.CurrentRow.DataBoundItem as DTO_Clientes;
                CadastroCliente tela = new CadastroCliente();
                tela.LoadScreenAlterar();


            }
        }
    }
}
