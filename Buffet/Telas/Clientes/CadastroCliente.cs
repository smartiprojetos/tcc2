﻿using Buffet.Db.Clientes;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroCliente : Form
    {
        public CadastroCliente()
        {
            InitializeComponent();
            UF uf = new UF();
            cboUF.DataSource = uf.UFS();
        }
        DTO_Clientes dto = new DTO_Clientes();

        public void LoadScreenAlterar()
        {
            txtid.Text = dto.ID.ToString();
            txtcliente.Text = dto.Nome;
            txtcpf.Text = dto.CPF;
            dtpnascimento.Value = dto.Data_Nascimento;
            dtpcadastro.Value = dto.Data_Cadastro;
            txtobs.Text = dto.Observacao_Cadastro;
            txtcep.Text = dto.CEP;
            cboUF.SelectedItem = dto.UF;
            txtcidade.Text = dto.Cidade;
            txtendereco.Text = dto.Endereco;
            txtnumero.Text = dto.Numero;
            txtcomplemento.Text = dto.Complemento;
            txtbairro.Text = dto.Bairro;
            txtobsendereco.Text = dto.Observacoes_Endereco;
            txttelefone.Text = dto.Telefone;
            txtcelular.Text = dto.Celular;
            txtemail.Text = dto.Email;

            btnalterar.Enabled = true;
            btnsalvar.Enabled = false;
            txtid.Enabled = false;

        }
        private void CarregarDados()
        {
            dto.Nome = txtid.Text;
            dto.CPF = txtcpf.Text;
            dto.Data_Nascimento = dtpnascimento.Value;
            dto.Data_Cadastro = dtpcadastro.Value;
            dto.Observacao_Cadastro = txtobs.Text;
            dto.CEP = txtcep.Text;
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Cidade = txtcidade.Text;
            dto.Endereco = txtendereco.Text;
            dto.Numero = txtnumero.Text;
            dto.Complemento = txtcomplemento.Text; ;
            dto.Bairro = txtbairro.Text;
            dto.Observacoes_Endereco = txtobsendereco.Text;
            dto.Telefone = txttelefone.Text;
            dto.Celular = txtcelular.Text;
            dto.Email = txtemail.Text;


        }
        Business_Cliente bus = new Business_Cliente();
        private void SalvarDados()
        {
            bus.Salvar(dto);
        }
        private void AlterarDados()
        {
            bus.Alterar(dto);
        }

        private void btnbuscarcep_Click(object sender, EventArgs e)
        {
            BuscarCep cep = new BuscarCep();
            cep.Buscarcep(txtcep.Text);

            CorreioResponse correio = new CorreioResponse();
            txtendereco.Text = correio.logradouro + " - " + correio.complemento;
            txtbairro.Text = correio.bairro;
            txtcidade.Text = correio.localidade;
            cboUF.SelectedItem = correio.uf;
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
           

        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            CarregarDados();
            SalvarDados();
            MessageBox.Show("Cliente cadastrado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CarregarDados();
            AlterarDados();


            MessageBox.Show("Cliente alterado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form1 inicio = new Form1();
            inicio.Show();
            this.Close();
                //Hide();

        }

        
    }
}
