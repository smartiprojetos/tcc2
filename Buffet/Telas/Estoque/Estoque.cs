﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Telas.EditarEstoque poi = new Telas.EditarEstoque();
            poi.Show();
            Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Telas.ConsultarFornecedores pe = new Telas.ConsultarFornecedores();
            pe.Show();
            Hide();
        }
    }
}
