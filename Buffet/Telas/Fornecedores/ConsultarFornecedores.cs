﻿using Buffet.Db.Fornecedores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarFornecedores : Form
    {
        public ConsultarFornecedores()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
        }
        private void Busca()
        {
            Business_Fornecedores bus = new Business_Fornecedores();
            if (txtbusca.Text == string.Empty)
            {
                bus.Listar();
                List<DTO_Fornecedores> lista = bus.Listar();
                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = lista;
            }
            

        }
        private void button3_Click(object sender, EventArgs e)
        {
            Busca();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Telas.PedidoFornecedor ee = new Telas.PedidoFornecedor();
            ee.Show();
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Telas.Estoque oo = new Telas.Estoque();
            oo.Show();
            Hide();
        }

        //Evento de clikar em algum lugar da grid 
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Business_Fornecedores business = new Business_Fornecedores();
            //Se a coluna 5 -- 
            if (e.ColumnIndex == 4)
            {
                //pega os valores da linha selecionada e envia para o dto 
                DTO_Fornecedores dto = dataGridView1.CurrentRow.DataBoundItem as DTO_Fornecedores;
                business.Remover(dto.ID);
            }
            if (e.ColumnIndex == 5)
            {
                DTO_Fornecedores dto = dataGridView1.CurrentRow.DataBoundItem as DTO_Fornecedores;
                CadastroFornecedores tela = new CadastroFornecedores();
                tela.LoadScreenAlterar();
            }
        }

        private void ConsultarFornecedores_Load(object sender, EventArgs e)
        {

        }
    }
}
