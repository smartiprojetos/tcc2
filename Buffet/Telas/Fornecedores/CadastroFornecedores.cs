﻿using Buffet.Db.Fornecedores;
using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroFornecedores : Form
    {
        public CadastroFornecedores()
        {
            InitializeComponent();
        }
       

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            DTO_Fornecedores fornecedor = new DTO_Fornecedores();
            int fk = UserSession.UsuarioLogado.fk_funcionario;
            fornecedor.nome_Fantasia = txtNomeFornecedor.Text;
            fornecedor.CPF_CNPJ = txtcpfcnpjfornecedor.Text;
            fornecedor.Data_Cadastro = dtpdatacadastro.Value;
            fornecedor.Data_Saida = dtpdatasaida.Value;
            fornecedor.Situacao = cbosituacao.SelectedItem.ToString();
            fornecedor.Data_Saida = dtpdatasaida.Value;
            fornecedor.observacao_Cadastro = txtobsCadastro.Text;
            fornecedor.CEP = txtCEP.Text;
            fornecedor.UF = cboUF.SelectedItem.ToString();
            fornecedor.cidade = txtCidade.Text;
            fornecedor.endereco = txtEndereço.Text;
            fornecedor.complemento = txtcomplemento.Text;
            fornecedor.bairro = txtBairro.Text;
            fornecedor.Numero = txtnumero.Text;
            fornecedor.observacoes_Endereco = txtObsEndereco.Text;
            fornecedor.telefone = txtTelefone.Text;
            fornecedor.celular = txtCelular.Text;
            fornecedor.email = txtEmail.Text;

            UF uf = new UF();
            cboUF.DataSource = uf.UFS();

            Business_Fornecedores business_Fornecedores = new Business_Fornecedores();
            business_Fornecedores.Salvar(fornecedor);

            MessageBox.Show("Fornecedor Cadastrado Com Sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);



            Hide();

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Fornecedor Alterado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Hide();
        }

        private void label25_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void label21_Click_1(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }

        public void LoadScreenAlterar()
        {
            DTO_Fornecedores dto = new DTO_Fornecedores();
           txtNomeFornecedor.Text= dto.nome_Fantasia;
             txtcpfcnpjfornecedor.Text = dto.CPF_CNPJ;
            dtpdatacadastro.Value = dto.Data_Cadastro;
           dtpdatasaida.Value = dto.Data_Saida;
             cbosituacao.SelectedItem = dto.Situacao;
            dtpdatasaida.Value = dto.Data_Saida; ;
             txtobsCadastro.Text = dto.observacao_Cadastro;
            txtCEP.Text = dto.CEP;
            cboUF.SelectedItem = dto.UF;
            txtCidade.Text = dto.cidade;
             txtEndereço.Text = dto.endereco;
            txtcomplemento.Text = dto.complemento;
            txtBairro.Text = dto.bairro;
             txtnumero.Text = dto.Numero;
           txtObsEndereco.Text = dto.observacoes_Endereco;
            txtTelefone.Text = dto.telefone;
            txtCelular.Text = dto.celular;
             txtEmail.Text = dto.email;

            btnalterar.Enabled = true;
            btnsalvar.Enabled = false;
            lbltitulo.Text = "Alterar Fornecedor";




        }

        private void CadastroFornecedores_Load(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
