﻿using Buffet.Db.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet
{
    public partial class Telainicial : Form
    {
        public Telainicial()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Business_Funcionario business = new Business_Funcionario();
            DTO_Fucionario dto = business.Logar(txtusuario.Text, txtsenha.Text);

            if (dto != null)
            {
                UserSession.UsuarioLogado = dto;
                Form1 menu = new Form1();
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Dados inválidos",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtusuario_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
