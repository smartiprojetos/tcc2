﻿namespace Buffet.Telas
{
    partial class REalizarPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidusuario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCPFcliente = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtidfesta = new System.Windows.Forms.TextBox();
            this.ID_Festa = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtnomecliente = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label19 = new System.Windows.Forms.Label();
            this.txtdescricao = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtlocal = new System.Windows.Forms.TextBox();
            this.dtpDataFesta = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cboFormaPagar = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtvalorTotal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtvalor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtdesconto = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(627, 30);
            this.panel2.TabIndex = 166;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(604, 5);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 19);
            this.label21.TabIndex = 83;
            this.label21.Text = "X";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(5, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(312, 19);
            this.label20.TabIndex = 49;
            this.label20.Text = "Magic Buffet - Realizar Reserva de Festa";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 414);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(627, 10);
            this.panel1.TabIndex = 171;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 172;
            this.label1.Text = "Usuário:";
            // 
            // txtidusuario
            // 
            this.txtidusuario.Location = new System.Drawing.Point(58, 3);
            this.txtidusuario.Name = "txtidusuario";
            this.txtidusuario.Size = new System.Drawing.Size(27, 20);
            this.txtidusuario.TabIndex = 173;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 174;
            this.label2.Text = "C.P.F. Cliente";
            // 
            // txtCPFcliente
            // 
            this.txtCPFcliente.Location = new System.Drawing.Point(11, 20);
            this.txtCPFcliente.Name = "txtCPFcliente";
            this.txtCPFcliente.Size = new System.Drawing.Size(291, 20);
            this.txtCPFcliente.TabIndex = 175;
            this.txtCPFcliente.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.txtidfesta);
            this.panel3.Controls.Add(this.ID_Festa);
            this.panel3.Controls.Add(this.txtidusuario);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(-1, 29);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(629, 30);
            this.panel3.TabIndex = 176;
            // 
            // txtidfesta
            // 
            this.txtidfesta.Location = new System.Drawing.Point(194, 3);
            this.txtidfesta.Name = "txtidfesta";
            this.txtidfesta.Size = new System.Drawing.Size(27, 20);
            this.txtidfesta.TabIndex = 195;
            // 
            // ID_Festa
            // 
            this.ID_Festa.AutoSize = true;
            this.ID_Festa.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID_Festa.Location = new System.Drawing.Point(91, 6);
            this.ID_Festa.Name = "ID_Festa";
            this.ID_Festa.Size = new System.Drawing.Size(101, 17);
            this.ID_Festa.TabIndex = 194;
            this.ID_Festa.Text = "Festa Número:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.txtnomecliente);
            this.panel4.Controls.Add(this.txtCPFcliente);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(-8, 58);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(640, 59);
            this.panel4.TabIndex = 177;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(322, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 17);
            this.label7.TabIndex = 177;
            this.label7.Text = "Nome Cliente";
            // 
            // txtnomecliente
            // 
            this.txtnomecliente.Location = new System.Drawing.Point(325, 20);
            this.txtnomecliente.Name = "txtnomecliente";
            this.txtnomecliente.Size = new System.Drawing.Size(291, 20);
            this.txtnomecliente.TabIndex = 176;
            this.txtnomecliente.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 125);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(132, 17);
            this.label19.TabIndex = 180;
            this.label19.Text = "Descrição Da Festa";
            // 
            // txtdescricao
            // 
            this.txtdescricao.Location = new System.Drawing.Point(11, 151);
            this.txtdescricao.Multiline = true;
            this.txtdescricao.Name = "txtdescricao";
            this.txtdescricao.Size = new System.Drawing.Size(306, 89);
            this.txtdescricao.TabIndex = 179;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 249);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 181;
            this.label5.Text = "Local Da Festa";
            // 
            // txtlocal
            // 
            this.txtlocal.Location = new System.Drawing.Point(9, 275);
            this.txtlocal.Multiline = true;
            this.txtlocal.Name = "txtlocal";
            this.txtlocal.Size = new System.Drawing.Size(268, 50);
            this.txtlocal.TabIndex = 182;
            // 
            // dtpDataFesta
            // 
            this.dtpDataFesta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dtpDataFesta.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDataFesta.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtpDataFesta.Location = new System.Drawing.Point(9, 360);
            this.dtpDataFesta.Name = "dtpDataFesta";
            this.dtpDataFesta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtpDataFesta.RightToLeftLayout = true;
            this.dtpDataFesta.Size = new System.Drawing.Size(271, 22);
            this.dtpDataFesta.TabIndex = 184;
            this.dtpDataFesta.Value = new System.DateTime(2018, 9, 11, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 334);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 183;
            this.label4.Text = "Data da Festa";
            this.label4.Click += new System.EventHandler(this.label4_Click_1);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Location = new System.Drawing.Point(340, 118);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 280);
            this.panel5.TabIndex = 178;
            // 
            // cboFormaPagar
            // 
            this.cboFormaPagar.FormattingEnabled = true;
            this.cboFormaPagar.Items.AddRange(new object[] {
            "Cartão de Crédito",
            "Cartão de Débito",
            "Boleto",
            "Cheque",
            "Dinheiro",
            "Carnê",
            "Boleto Bancario",
            ""});
            this.cboFormaPagar.Location = new System.Drawing.Point(375, 277);
            this.cboFormaPagar.Name = "cboFormaPagar";
            this.cboFormaPagar.Size = new System.Drawing.Size(217, 21);
            this.cboFormaPagar.TabIndex = 186;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(375, 260);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 17);
            this.label10.TabIndex = 185;
            this.label10.Text = "Forma De Pagamento";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(375, 216);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 21);
            this.label16.TabIndex = 190;
            this.label16.Text = "Valor total";
            // 
            // txtvalorTotal
            // 
            this.txtvalorTotal.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvalorTotal.Location = new System.Drawing.Point(375, 237);
            this.txtvalorTotal.Name = "txtvalorTotal";
            this.txtvalorTotal.Size = new System.Drawing.Size(219, 23);
            this.txtvalorTotal.TabIndex = 189;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(375, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 21);
            this.label15.TabIndex = 188;
            this.label15.Text = "Valor";
            // 
            // txtvalor
            // 
            this.txtvalor.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvalor.Location = new System.Drawing.Point(375, 149);
            this.txtvalor.Name = "txtvalor";
            this.txtvalor.Size = new System.Drawing.Size(219, 23);
            this.txtvalor.TabIndex = 187;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(375, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 21);
            this.label6.TabIndex = 192;
            this.label6.Text = "Desconto";
            // 
            // txtdesconto
            // 
            this.txtdesconto.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdesconto.Location = new System.Drawing.Point(375, 193);
            this.txtdesconto.Name = "txtdesconto";
            this.txtdesconto.Size = new System.Drawing.Size(219, 23);
            this.txtdesconto.TabIndex = 191;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(523, 370);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 17);
            this.label26.TabIndex = 227;
            this.label26.Text = "Alterar";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(452, 370);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 17);
            this.label27.TabIndex = 226;
            this.label27.Text = "Cancelar";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Buffet.Properties.Resources.alterar;
            this.pictureBox3.Location = new System.Drawing.Point(519, 334);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(58, 33);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 224;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(398, 370);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 17);
            this.label28.TabIndex = 225;
            this.label28.Text = "Salvar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Buffet.Properties.Resources._1024px_Deletion_icon_svg;
            this.pictureBox1.Location = new System.Drawing.Point(455, 334);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 222;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Buffet.Properties.Resources._678134_sign_check_512;
            this.pictureBox2.Location = new System.Drawing.Point(391, 334);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(58, 33);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 223;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // REalizarPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(627, 424);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtdesconto);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtvalorTotal);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtvalor);
            this.Controls.Add(this.cboFormaPagar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.dtpDataFesta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtlocal);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtdescricao);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "REalizarPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "REalizarPedido";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidusuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCPFcliente;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtdescricao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtlocal;
        private System.Windows.Forms.DateTimePicker dtpDataFesta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox cboFormaPagar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtvalorTotal;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtvalor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdesconto;
        private System.Windows.Forms.TextBox txtidfesta;
        private System.Windows.Forms.Label ID_Festa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtnomecliente;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}