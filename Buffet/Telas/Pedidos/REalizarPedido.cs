﻿using Buffet.Db.Funcionarios;
using Buffet.Db.Pedidos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class REalizarPedido : Form
    {
        public REalizarPedido()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            Telas.TesteLogin oi8 = new Telas.TesteLogin();
            oi8.Show();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {
            

            Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            DTO_Pedidos dto = new DTO_Pedidos();
            
            dto.Data_Agendamento = DateTime.Now;
            dto.data_Festa = dtpDataFesta.Value;
            dto.desconto = Convert.ToDecimal( txtdesconto.Text);
            dto.descricao = txtdescricao.Text;
            dto.local = txtlocal.Text;
            dto.Valor = Convert.ToDecimal(txtvalor.Text);
            dto.Valor_Total = Convert.ToDecimal(txtvalorTotal.Text);
            dto.forma_Pagamento = cboFormaPagar.Text;

            int pk = UserSession.UsuarioLogado.ID_Funcionario;
            dto.fk_Funcionario = Convert.ToInt32(txtidusuario.Text);


            Business_Pedidos business = new Business_Pedidos();
            business.Salvar(dto);


            MessageBox.Show("Reserva efetuada com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Reserva atualizada com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Hide();
        }
    }
}
