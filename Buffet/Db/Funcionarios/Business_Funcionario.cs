﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Funcionarios
{
    class Business_Funcionario
    {

        public int Salvar(DTO_Fucionario dto)
        {
            Database_Funcionario db = new Database_Funcionario();
            int pk = db.Salvar(dto);
            return pk;
        }

        public DTO_Fucionario Logar (string usuario, string senha)
        {
            if (usuario == string.Empty)
            {
                throw new ArgumentException("O usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("A senha é obrigatória.");
            }
            Database_Funcionario db = new Database_Funcionario();
            return db.Logar(usuario, senha);
        }

    }
}

