﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Terceirizados
{

    class Database_Terceirizados
    {
        Database db = new Database();

        public int Salvar(DTO_Terceirizados dto)
        {
            string script =
                @"INSERT 
                  INTO 
                  tercerizados (
                                ID_Usuario,
                                Nome_Fantasia,
                                CPF_CNPJ,
                                Situacao,
                                Data_Cadastro,
                                Data_Saida,
                                Telefone1,
                                Celular,
                                Email,
                                observacoes) 
                         values(
                                @Nome_Fantasia,
                                @ID_Usuario,
                                @CPF_CNPJ,
                                @Situacao,
                                @Data_Cadastro,
                                @Data_Saida,
                                @Telefone1,
                                @Celular,
                                @Email,
                                @observacoes) 
                                ";

           List< MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Usuario",dto.ID_Usuario));
            parms.Add(new MySqlParameter("Nome_Fantasia", dto.nome_Fantasia));
            parms.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parms.Add(new MySqlParameter("Situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parms.Add(new MySqlParameter("Telefone1", dto.telefone));
            parms.Add(new MySqlParameter("Celular", dto.celular));
            parms.Add(new MySqlParameter("Email", dto.email));
            parms.Add(new MySqlParameter("observacoes", dto.observacoes));

            return db.ExecuteInsertScriptWithPk(script, parms);
            

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tercerizWHERE ID_Terceiros = @ID_Terceiros";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Terceiros", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(DTO_Terceirizados dto)
        {
            string script = @"UPDATE tercerizados SET
                             ID_Terceiros  = @ID_Terceiros
                             ID_Usuario    = @Nome_Fantasia,
                             Nome_Fantasia = @ID_Usuario,
                             CPF_CNPJ      = @CPF_CNPJ,
                             Situacao      = @Situacao,
                             Data_Cadastro = @Data_Cadastro,
                             Data_Saida    = @Data_Saida,
                             Telefone1     = @Telefone1,
                             Celular       = @Celular,
                             Email         = @Email,
                             observacoes   = @observacoes";
            
                              
                                

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Usuario", dto.ID_Usuario));
            parms.Add(new MySqlParameter("Nome_Fantasia", dto.nome_Fantasia));
            parms.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parms.Add(new MySqlParameter("Situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parms.Add(new MySqlParameter("Telefone1", dto.telefone));
            parms.Add(new MySqlParameter("Celular", dto.celular));
            parms.Add(new MySqlParameter("Email", dto.email));
            parms.Add(new MySqlParameter("observacoes", dto.observacoes));

            db.ExecuteInsertScript(script,parms);

        }

        public List<DTO_Terceirizados> Listar()
        {
            string script = "Select * from tercerizados";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            List<DTO_Terceirizados> lista = new List<DTO_Terceirizados>();
            while (reader.Read())
            {
                DTO_Terceirizados dto = new DTO_Terceirizados();
                dto.ID = reader.GetInt32("ID_Terceiros");
                dto.ID_Usuario = reader.GetInt32("ID_Usuario");
                dto.nome_Fantasia = reader.GetString("Nome_Fantasia");
                dto.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                dto.Situacao = reader.GetString("Situacao");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.telefone = reader.GetString("Telefone1");
                dto.celular = reader.GetString(" Celular");
                dto.email = reader.GetString(" Email");
                dto.observacoes = reader.GetString("observacoes");

                lista.Add(dto);

            }
            reader.Close();

            return lista;

        }

        public List<DTO_Terceirizados> Consultar(string tercerizado)
        {
            string script = @"SELECT * FROM tercerizados WHERE Nome_Fantasia like @Nome_Fantasia";

            List<MySqlParameter> parms = new List<MySqlParameter>();
             parms.Add(new MySqlParameter("Nome_Fantasia", tercerizado + "%"));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Terceirizados> lista = new List<DTO_Terceirizados>();
            while (reader.Read())
            {
                DTO_Terceirizados dto = new DTO_Terceirizados();
                dto.ID = reader.GetInt32("ID_Terceiros");
                dto.ID_Usuario = reader.GetInt32("ID_Usuario");
                dto.nome_Fantasia = reader.GetString("Nome_Fantasia");
                dto.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                dto.Situacao = reader.GetString("Situacao");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.telefone = reader.GetString("Telefone1");
                dto.celular = reader.GetString(" Celular");
                dto.email = reader.GetString(" Email");
                dto.observacoes = reader.GetString("observacoes");

                lista.Add(dto);

            }
            reader.Close();

            return lista;

        }

    }
}
