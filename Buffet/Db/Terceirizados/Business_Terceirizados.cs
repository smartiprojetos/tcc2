﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Terceirizados
{
    class Business_Terceirizados
    {
        Database_Terceirizados db = new Database_Terceirizados();

        public int Salvar(DTO_Terceirizados dto)
        {
       
            if (dto.nome_Fantasia == string.Empty || dto.nome_Fantasia.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatorio");
            }
            if (dto.CPF_CNPJ == string.Empty || dto.CPF_CNPJ.Trim() == string.Empty)
            {
                throw new ArgumentException("CPF/CNPJ é obrigatorio");
            }
            if (dto.Situacao == string.Empty || dto.Situacao.Trim() == string.Empty)
            {
                throw new ArgumentException("Situação é obrigatoria");
            }
            if (dto.Data_Cadastro == dto.Data_Saida)
            {
                throw new ArgumentException("Um funcionario não pode ser demetido dentro de 24hs");
            }
            if (dto.telefone == string.Empty || dto.celular == string.Empty)
            {
                throw new ArgumentException("Celular ou Telefone são obrigatorios");
            }
            if (dto.email == string.Empty || dto.email.Trim() == string.Empty)
            {
                throw new ArgumentException("Email é obrigatorio");
            }

            return  db.Salvar(dto);
            
        }

        public void Alterar (DTO_Terceirizados dto)
        {
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
        public List<DTO_Terceirizados> Listar()
        {
            return db.Listar();
        }

        
    }
}
