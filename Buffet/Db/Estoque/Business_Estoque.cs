﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Estoque
{
    class Business_Estoque
    {
        Database_Estoque db = new Database_Estoque();

       public int Salvar (DTO_Estoque dto)
        {
           return db.Salvar(dto);
        }

        public List<DTO_Estoque> Listar()
        {
            return db.Listar();
        }

        public List<DTO_Estoque>Listar (string produto)
        {
            return db.Listar(produto);
        }
        
    }
} 
