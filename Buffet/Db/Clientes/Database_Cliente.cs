﻿using Buffet.Db.Base;
using Buffet.Db.Clientes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Clientes
{
    class Database_Cliente
    {
        public int Salvar(DTO_Clientes dto)
        {

            string script = @"Insert into cliente (Nome,
                                                      CPF,
                                                    Data_Nascimento,
                                                    Data_Cadastro,
                                                    observacao_Cadastro,
                                                    CEP,
                                                    UF,
                                                    Cidade,
                                                    Endereco,
                                                    Complemento,
                                                    Bairro,
                                                    Numero,
                                                    Observacoes_Endereco,
                                                    Telefone,
                                                    Celular,
                                                    Email
                                                    
                                                    ) 
                                                    Values
                                                    (@Nome,
                                                      @CPF,
                                                    @Data_Nascimento,
                                                    @Data_Cadastro,
                                                    @observacao_Cadastro,
                                                    @CEP,
                                                    @UF,
                                                   @Cidade,
                                                    @Endereco,
                                                    @Complemento,
                                                    @Bairro,
                                                    @Numero,
                                                    @Observacoes_Endereco,
                                                    @Telefone,
                                                    @Celular,
                                                    @Email
                                                   
                                                    )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("Data_Nascimento", dto.Data_Nascimento));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("Observacao_Cadastro", dto.Observacao_Cadastro));
            parms.Add(new MySqlParameter("CEP", dto.CEP));
            parms.Add(new MySqlParameter("UF", dto.UF));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Complemento", dto.Complemento));
            parms.Add(new MySqlParameter("Bairro", dto.Bairro));
            parms.Add(new MySqlParameter("Numero", dto.Numero));
            parms.Add(new MySqlParameter("Observacoes_Endereco", dto.Observacoes_Endereco));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Celular", dto.Celular));
            parms.Add(new MySqlParameter("Email", dto.Email));
           

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);         

        }

        public void Remover(int id) {
            string script = @"DELETE FROM  cliente WHERE ID_Cliente = @ID_Fornecedor";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Cliente", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }

        public void Alterar(DTO_Clientes dto )
        {
            string script = @"Update cliente SET
                                                Nome = @Nome,
                                                CPF = @CPF,
                                                Data_Nascimento = @Data_Nascimento,
                                                Data_Cadastro = @Data_Cadastro ,
                                                Observacao_Cadastro = @Observacao_Cadastro ,
                                                CEP = @CEP ,
                                                UF = @UF ,
                                                Cidade = @Cidade ,
                                                Endereco = @Endereco ,
                                                Complemento = @Complemento ,
                                                Bairro = @Bairro ,
                                                Numero = @Numero ,
                                                Observacoes_Endereco = @Observacoes_Endereco ,
                                                Telefone = @Telefone ,
                                                Celular = @Celular ,
                                                Email = @Email 
                                               ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("Data_Nascimento", dto.Data_Nascimento));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("Observacao_Cadastro", dto.Observacao_Cadastro));
            parms.Add(new MySqlParameter("CEP", dto.CEP));
            parms.Add(new MySqlParameter("UF", dto.UF));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Complemento", dto.Complemento));
            parms.Add(new MySqlParameter("Bairro", dto.Bairro));
            parms.Add(new MySqlParameter("Numero", dto.Numero));
            parms.Add(new MySqlParameter("Observacoes_Endereco", dto.Observacoes_Endereco));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Celular", dto.Celular));
            parms.Add(new MySqlParameter("Email", dto.Email));

            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, parms);


        }

        public List<DTO_Clientes> Consultar(string nome)
        {
            string script = @"SELECT * FROM funcionario WHERE Nome like @Nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Clientes> lista = new List<DTO_Clientes>();
            while (reader.Read())
            {
                DTO_Clientes dto = new DTO_Clientes();
                dto.ID = reader.GetInt32("ID");
                dto.Nome = reader.GetString("Nome");
                dto.CPF = reader.GetString("CPF");
                dto.Data_Nascimento = reader.GetDateTime("Data_Nascimento");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Observacao_Cadastro = reader.GetString("Observacao_Cadastro");
                dto.CEP = reader.GetString("CEP");
                dto.UF = reader.GetString("UF");
                dto.Cidade = reader.GetString("Cidade");
                dto.Endereco = reader.GetString("Endereco");
                dto.Complemento = reader.GetString("Complemento");
                dto.Bairro = reader.GetString("Bairro");

                dto.Numero = reader.GetString("Numero");

                dto.Observacoes_Endereco = reader.GetString("Observacoes_Endereco");
                dto.Telefone = reader.GetString("Telefone");
                dto.Celular = reader.GetString("Celular");
                dto.Email = reader.GetString("Email");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_Clientes> Listar()
        {

            string script = @"SELECT * FROM funcionario ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Clientes> lista = new List<DTO_Clientes>();
            while (reader.Read())
            {
                DTO_Clientes dto = new DTO_Clientes();
                dto.ID = reader.GetInt32("ID");
                dto.Nome = reader.GetString("Nome");
                dto.CPF = reader.GetString("CPF");
                dto.Data_Nascimento = reader.GetDateTime("Data_Nascimento");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Observacao_Cadastro = reader.GetString("Observacao_Cadastro");
                dto.CEP = reader.GetString("CEP");
                dto.UF = reader.GetString("UF");
                dto.Cidade = reader.GetString("Cidade");
                dto.Endereco = reader.GetString("Endereco");
                dto.Complemento = reader.GetString("Complemento");
                dto.Bairro = reader.GetString("Bairro");

                dto.Numero = reader.GetString("Numero");

                dto.Observacoes_Endereco = reader.GetString("Observacoes_Endereco");
                dto.Telefone = reader.GetString("Telefone");
                dto.Celular = reader.GetString("Celular");
                dto.Email = reader.GetString("Email");
                

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

    }
}
