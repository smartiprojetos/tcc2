﻿using Buffet.Db.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Clientes
{
    class Business_Cliente
    {
        Database_Cliente db = new Database_Cliente();

        public int Salvar(DTO_Clientes dto)
        {
           
            int pk = db.Salvar(dto);
            return pk;
        }

        public void Alterar(DTO_Clientes dto)
        {
            db.Alterar(dto);

        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<DTO_Clientes> Consultar(string produto )
        {
            return db.Consultar(produto);
        }

        public List<DTO_Clientes> Listar()
        {
            return db.Listar();
        }
    }
}
