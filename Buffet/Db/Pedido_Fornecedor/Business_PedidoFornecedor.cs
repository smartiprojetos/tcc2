﻿using Buffet.Db.Pedido_Fornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores.Pedido_Fornecedor
{
    class Business_PedidoFornecedor
    {
        Database_PedidoFornecedor Database_PedidoFornecedor = new Database_PedidoFornecedor();

        public int Salvar(DTO_Pedidofornecedor dto)
        {

            int pk = Database_PedidoFornecedor.Salvar(dto);
            return pk;
        }

        public void Alterar(DTO_Pedidofornecedor dto)
        {
            Database_PedidoFornecedor.Alterar(dto);
        }

        public void Remover(int id)
        {
            Database_PedidoFornecedor.Remover(id);
        }

        public List<DTO_Pedidofornecedor> Listar()
        {
            List<DTO_Pedidofornecedor> lista = Database_PedidoFornecedor.Listar();
            return lista;
        }

        public List<DTO_Pedidofornecedor> Consultar(string Produto)
        {
            List<DTO_Pedidofornecedor> lista = Database_PedidoFornecedor.Consultar(Produto);
            return lista; 
        }
        public List<DTO_Pedidofornecedor> Buscar(int id)
        {
            List<DTO_Pedidofornecedor> lista = Database_PedidoFornecedor.Buscar(id);
            return lista;

        }
    }
}
