﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Departamento
{
    class DTO_Departamento
    {
        public int id { get; set; }
        public string nm_departamento { get; set; }
        public bool nm_permissao { get; set; }


    }
}
