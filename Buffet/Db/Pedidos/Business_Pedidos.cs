﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class Business_Pedidos
    {


        Database_Pedidos database_Pedidos = new Database_Pedidos();
        public int Salvar(DTO_Pedidos dto)
        {
            Database_Pedidos db = new Database_Pedidos();
            int pk = db.Salvar(dto);
            return pk;
        }

        public void Alterar(DTO_Pedidos dto)
        {
            database_Pedidos.Alterar(dto);
        }

        public void Remover(int id)
        {
            database_Pedidos.Remover(id);
        }
        

    }
}
