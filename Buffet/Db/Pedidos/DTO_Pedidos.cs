﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class DTO_Pedidos

    {

        public int id { get; set; }
        public DateTime Data_Agendamento { get; set; }
        public string descricao { get; set; }
        public string local { get; set; }
        public DateTime data_Festa {get; set; }
        public decimal Valor_Total { get; set; }
        public decimal Valor { get; set; }
        public decimal desconto { get; set; }
        public string forma_Pagamento { get; set; }

        public int fk_Funcionario { get; set; }


    }
}
