﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class Database_Pedidos
    {
        public int Salvar(DTO_Pedidos dto)
        {
            string script = @"Insert into   pedido_festa ( 
                                                             Data_Agendamento, 
                                                             Descricao, 
                                                             local , 
                                                             data_Festa ,
                                                             Valor_Total,        
                                                             Valor,              
                                                             desconto,
                                                             forma_pagamento,
                                                             fk_Funcionario 
    
                                                                ) Values (@id,
                                                                    @Data_Agendamento,
                                                                    @Descricao, 
                                                                    @local,
                                                                    @data_Festa,
                                                                    @Valor_Total,
                                                                    @Valor,
                                                                    @desconto,
                                                                    @forma_Pagamento
                                                                    @fk_Funcionario)  ";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("Data_Agendamento", dto.Data_Agendamento));
            parm.Add(new MySqlParameter("Descricao", dto.descricao));
            parm.Add(new MySqlParameter("local", dto.local));
            parm.Add(new MySqlParameter("data_Festa", dto.data_Festa));
            parm.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parm.Add(new MySqlParameter("Valor", dto.Valor));
            parm.Add(new MySqlParameter("desconto", dto.desconto));
            parm.Add(new MySqlParameter("forma_Pagamento", dto.forma_Pagamento));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parm);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM pedido_festa WHERE ID_PedidoFesta = @ID_PedidoFesta";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PedidoFesta", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(DTO_Pedidos dto)
        {
            string script = @" update pedido_festa Set 
                                                        Data_Agendamento = @Data_Agendamento, 
                                                        Descricao = @Descricao,
                                                        local = @local,
                                                        data_Festa = @data_Festa,
                                                        Valor_Total = @Valor_Total, 
                                                        Valor = @Valor,
                                                        desconto = @desconto,
                                                        forma_Pagamento = @pagamento";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("Data_Agendamento", dto.Data_Agendamento));
            parm.Add(new MySqlParameter("Descricao", dto.descricao));
            parm.Add(new MySqlParameter("local", dto.local));
            parm.Add(new MySqlParameter("data_Festa", dto.data_Festa));
            parm.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parm.Add(new MySqlParameter("Valor", dto.Valor));
            parm.Add(new MySqlParameter("desconto", dto.desconto));
            parm.Add(new MySqlParameter("forma_Pagamento", dto.forma_Pagamento));

            Database db = new Database();
            db.ExecuteSelectScript(script, parm);



        }





    }
}
