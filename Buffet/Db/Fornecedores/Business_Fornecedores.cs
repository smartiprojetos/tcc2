﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores
{
    class Business_Fornecedores
    {
        //Instanciando em forma de escopo pois os metodos abaixo irão utilizar essa business
        Database_Fornecedores database_Fornecedores = new Database_Fornecedores();

        public int Salvar (DTO_Fornecedores dto)
        {          
            int pk = database_Fornecedores.Salvar(dto);
            return pk;
        }

        public void Alterar(DTO_Fornecedores dto)
        {
            database_Fornecedores.Alterar(dto);
        }

        public void Remover(int id)
        {
            database_Fornecedores.Remover(id);
        }

        public List<DTO_Fornecedores> Listar()
        {
            List<DTO_Fornecedores> lista = database_Fornecedores.List();
            return lista;
        }
    }
}
