﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores
{
    class Database_Fornecedores
    {

        

        public int Salvar(DTO_Fornecedores dto)
        {

            string script = 
            @"INSERT INTO fornecedores
            (
                Nome_Fantasia,
                CPF_CNPJ,
                Situacao,
                Data_Cadastro,
                Data_Saida,
                Observacoes_Cadastro,
                Endereco,
                Bairro,
                Numero,
                Complemento,
                Cidade,
                UF,
                CEP ,
                Obervacoes_Endereco,
                Telefone,
                Celular,
                Email,
                ID_User
            )
            VALUES 
            (
                @Nome_Fantasia,
                @CPF_CNPJ,
                @Situacao,
                @Data_Cadastro,
                @Data_Saida,
                @Observacoes_Cadastro,
                @Endereco,
                @Bairro,
                @Numero,
                @Complemento,
                @Cidade,
                @UF,
                @CEP ,
                @Obervacoes_Endereco,
                @Telefone,
                @Celular,
                @Email,
                @ID_User
             ) ";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("Nome_Fantasia", dto.nome_Fantasia));
            parm.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parm.Add(new MySqlParameter("Situacao", dto.Situacao));
            parm.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parm.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parm.Add(new MySqlParameter("Observacoes_Cadastro", dto.observacoes_Endereco));
            parm.Add(new MySqlParameter("Endereco", dto.endereco));
            parm.Add(new MySqlParameter("Bairro", dto.bairro));
            parm.Add(new MySqlParameter("Numero", dto.Numero));
            parm.Add(new MySqlParameter("Complemento", dto.complemento));
            parm.Add(new MySqlParameter("Cidade", dto.cidade));
            parm.Add(new MySqlParameter("UF", dto.UF));
            parm.Add(new MySqlParameter("CEP", dto.CEP));
            parm.Add(new MySqlParameter("Obervacoes_Endereco", dto.observacoes_Endereco));
            parm.Add(new MySqlParameter("Telefone", dto.telefone));
            parm.Add(new MySqlParameter("Celular", dto.celular));
            parm.Add(new MySqlParameter("Email", dto.email));

            parm.Add(new MySqlParameter("ID_User", dto.ID_User));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        //public List<DTO_Fornecedores> Consultar { }

        public List<DTO_Fornecedores> List()
        {
            string script = @"Select * from fornecedores";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            List<DTO_Fornecedores> lista = new List<DTO_Fornecedores>();
            while (reader.Read())
            {
                DTO_Fornecedores dto = new DTO_Fornecedores();
                dto.ID = reader.GetInt32("ID_Fornecedor");
                dto.bairro = reader.GetString("Bairro");
                dto.celular = reader.GetString("Celular");
                dto.CEP = reader.GetString("CEP");
                dto.cidade = reader.GetString("Cidade");
                dto.complemento = reader.GetString("Complemento");
                dto.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.email = reader.GetString("Email");
                dto.endereco = reader.GetString("Endereco");
      
                dto.ID_User = reader.GetInt32("ID_User");
                dto.nome_Fantasia = reader.GetString("Nome_Fantasia");
                dto.Numero = reader.GetString("Numero");
                dto.observacao_Cadastro = reader.GetString("Observacoes_Cadastro");
                dto.observacoes_Endereco = reader.GetString("Obervacoes_Endereco");
                dto.Situacao = reader.GetString("Situacao");
                dto.telefone = reader.GetString("Telefone");
                dto.UF = reader.GetString("UF");

                lista.Add(dto);

            }
            reader.Close();
            return lista;                          

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM fornecedores WHERE ID_Fornecedor = @ID_Fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Fornecedor", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar (DTO_Fornecedores dto)
        {
            string script = @"Update fornecedores SET
                                            Nome_Fantasia = @Nome_Fantasia ,
                                            CPF_CNPJ = @CPF_CNPJ  ,
                                            Situacao = @Situacao ,
                                            Data_Cadastro = @Data_Cadastro ,
                                            Data_Saida = @Data_Saida ,
                                            Observacoes_Cadastro = @Observacoes_Cadastro ,
                                            Endereco = @Endereco ,
                                            Bairro = @Bairro ,
                                            Numero= @Numero,
                                            Complemento = @Complemento ,
                                            Cidade = @Cidade ,
                                            UF = @UF ,
                                            CEP = @CEP ,
                                            Obervacoes_Endereco = @Obervacoes_Endereco ,
                                            Telefone = @Telefone  ,
                                            Celular = @Celular ,
                                            Email = @Email ,
                                            ID_User = @ID_User, 
                                            Where ID_Fornecedor = @ID_Fornecedor ";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            parm.Add(new MySqlParameter("Nome_Fantasia", dto.nome_Fantasia));
            parm.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parm.Add(new MySqlParameter("Situacao", dto.Situacao));
            parm.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parm.Add(new MySqlParameter(" Data_Saida", dto.Data_Saida));
            parm.Add(new MySqlParameter("Observacoes_Cadastro", dto.observacoes_Endereco));
            parm.Add(new MySqlParameter("Endereco", dto.endereco));
            parm.Add(new MySqlParameter("Bairro", dto.bairro));
            parm.Add(new MySqlParameter("Numero", dto.Numero));
            parm.Add(new MySqlParameter("Complemento", dto.complemento));
            parm.Add(new MySqlParameter("Cidade", dto.cidade));
            parm.Add(new MySqlParameter("UF", dto.UF));
            parm.Add(new MySqlParameter("CEP", dto.CEP));
            parm.Add(new MySqlParameter("Obervacoes_Endereco", dto.observacoes_Endereco));
            parm.Add(new MySqlParameter("Telefone", dto.telefone));
            parm.Add(new MySqlParameter("Celular", dto.celular));
            parm.Add(new MySqlParameter("Email", dto.email));
            parm.Add(new MySqlParameter("ID_User", dto.ID_User));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);

        }


    }
}