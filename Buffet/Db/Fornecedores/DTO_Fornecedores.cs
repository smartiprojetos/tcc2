﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores
{
    class DTO_Fornecedores
    {

        public int ID { get; set; }
        public string nome_Fantasia { get; set; }
        public string CPF_CNPJ { get; set; }
        public string Situacao { get; set; }
        public DateTime Data_Cadastro { get; set; }
        public DateTime Data_Saida { get; set; }
        public string observacao_Cadastro { get; set; }
        public string CEP { get; set; }
        public string UF { get; set; }
        public string cidade { get; set; }
        public string endereco { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string Numero { get; set; }
        public string observacoes_Endereco { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
        public int ID_User { get; set; }
 
        

    }
}
